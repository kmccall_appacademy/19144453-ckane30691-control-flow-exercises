# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  result = ""
  str.split("").each do |letter|
    result += letter if letter.upcase == letter
  end
  result
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def str_length_odd?(str)
  str.length % 2 == 1 ? true: false
end

def middle_substring(str)
  if str_length_odd?(str) 
    str[str.length / 2]
  else
    str[(str.length - 2) / 2] + str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.split("").each {|letter| count += 1 if VOWELS.include?(letter)}
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each do |item|
    item == arr.last ? result += item: result += item + separator
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ""
  str.split("").each_with_index do |letter, i|
    i % 2 == 0 ? result += letter.downcase: result += letter.upcase
  end
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []
  str.split(" ").each do |word|
    word.length >= 5 ? result << word.reverse: result << word
  end
  result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).each do |num|
    if num % 15 == 0
      result << "fizzbuzz"
    elsif num % 5 == 0
      result << "buzz"
    elsif num % 3 == 0
      result << "fizz"
    else
      result << num
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  count = arr.length - 1
  arr.each do |item|
    result << arr[count]
    count -= 1
  end
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  (2...num).each do |idx|
    if num % idx == 0
      return false
    end
  end
  num > 1 ? true: false
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (1..num).each {|idx| result << idx if num % idx == 0}
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  result = []
  factors(num).each {|idx| result << idx if prime?(idx)}
  result
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_arr = []
  even_arr = []
  arr.each do |num|
    num % 2 == 1 ? odd_arr << num: even_arr << num
  end
  odd_arr.length < even_arr.length ? odd_arr[0]: even_arr[0]
end
